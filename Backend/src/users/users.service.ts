import { Injectable } from '@nestjs/common';

export type User = any

@Injectable()
export class UsersService {
    private readonly users : User[]

    constructor() {
        this.users = [
            {
                id : '1',
                username : 'john',
                password : 'wick'
            },
            {
                id : '2',
                username : 'bobby',
                password : 'boys'
            },
            {
                id : '3',
                username : 'marry',
                password : 'johnson'
            }
        ]
    }

    async findOne(username : string) : Promise<User | undefined> {
        return this.users.find(user => user.username === username)
    }
}
