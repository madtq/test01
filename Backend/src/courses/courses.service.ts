import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { CreateCourseDto } from "./dto/create-course.dto";
import { Review } from "./review.entity";
import Course from "./course.entities";
import { CreateReviewDto } from "./dto/create-review.dto";
import { ObjectID } from "mongodb";

@Injectable()
export class CoursesService {
    constructor(
        @InjectRepository(Course) 
        private courseRepository: Repository<Course>,
        @InjectRepository(Review) 
        private reviewsRepository: Repository<Review>
    ){}
    
    async findAll() : Promise<Course[]> {
        return this.courseRepository.find()
    }

    async create(CreateCourseDto : CreateCourseDto) {
        return this.courseRepository.save(CreateCourseDto)
    }

    async findAllReviews(courseId : ObjectID) : Promise<Review[]> {
        return this.reviewsRepository.find({where : {courseId : courseId}})
    }

    async createReview(createReviewDto : CreateReviewDto) {
        return this.reviewsRepository.save(createReviewDto)
    }
}