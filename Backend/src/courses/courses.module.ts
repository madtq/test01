import { Module } from "@nestjs/common";
import { CoursesController } from "./courses.controller";
import { CoursesService } from "./courses.service";
import Course from "./course.entities";
import Review from "./review.entity";
import { TypeOrmModule } from "@nestjs/typeorm";

@Module ({
    imports : [TypeOrmModule.forFeature([Course,Review])],
    controllers : [CoursesController],
    providers : [CoursesService],
})

export class CoursesModule{}