import React, { useEffect, useState } from 'react';
import { Course } from '../interface';
import CourseItem from './CourseItem';
import NewCourseFrom from './NewCourseFrom';
import CoursesService from '../services/CoursesService';

const CourseReview = () => {
  const [courses,setCourses] = useState<Course[]>([]);
  const [fromVisible,setFromVisible] = useState<boolean>(false);

  const toggleFromVisible = () => {
    setFromVisible(!fromVisible)
  }

  const fetchCourses = () => {
    CoursesService.fetchCourses()
    .then (courses => {
      setCourses(courses)
    })
  }

  const handleNewCourseCreate = (course : Course) => {
    fetchCourses()
    setFromVisible(false)
  }

  useEffect(() => {
    fetchCourses()
  },[]);

  return(
    <div className = 'CourseReview'>
      <ul>
        {courses.map(item => (
          <CourseItem key = {item.id} course = {item}/>
        ))}
      </ul>
      <button onClick = {toggleFromVisible}>New Course</button>
      {fromVisible && 
        <NewCourseFrom onNewCourseCreate = {handleNewCourseCreate}/> 
      }
    </div>
  )
} 

export default CourseReview;
