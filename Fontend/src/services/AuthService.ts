import { baseUrl } from "../config/const";

async function loginUser(username : string, password : string) : Promise<any | null>{
    const res = await fetch(`${baseUrl}/auth/login`,{
        method : 'POST',
        headers : {'Content-Type' : 'application/json'},
        body : JSON.stringify({
            username : username,
            password : password
        })
    })
    const result = await res.json()
    if (result.accessToken){
        localStorage.setItem("user",result)
        return result
    }else {
        return null
    }
    console.log(result)
}

export default{
    loginUser
}